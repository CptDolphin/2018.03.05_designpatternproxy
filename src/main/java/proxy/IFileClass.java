package proxy;

import java.util.List;

public interface IFileClass {
    void skipLines(int howMuch) ;
    List<String> printLines(int howMuch) ;
}
