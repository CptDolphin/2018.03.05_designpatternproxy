package proxy;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileClass implements IFileClass {
    private BufferedReader bufferedReader;
    private int counter = 0;

    public FileClass() {
        openFile();
    }

    public void openFile() {
        try {
            bufferedReader = new BufferedReader(new FileReader("test.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String readOneLine() {
        try {
            String pom = bufferedReader.readLine();
            if (pom == null) {
                scrollFile();
                return bufferedReader.readLine();
            } else return pom;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void closeFile() {
        try {
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void skipLine() {
        readOneLine();
    }

    public void scrollFile() {
        closeFile();
        openFile();
    }

    public void skipLines(int howMuch) {
        for (int i = 0; i < howMuch; i++) {
            skipLine();
        }
    }

    public List<String> printLines(int howMuch) {
        List<String> stringList = new ArrayList<String>();
        for (int i = 0; i < howMuch; i++) {
            stringList.add(readOneLine());
            counter++;
        }
        return stringList;
    }
}
