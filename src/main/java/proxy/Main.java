package proxy;

public class Main {
    public static void main(String[] args) {
        IFileClass file = new FileClass();
        System.out.println(file.printLines(5).toString());
        file.skipLines(2);
        System.out.println(file.printLines(2).toString());
        file.skipLines(10);
        System.out.println(file.printLines(1).toString());
        System.out.println(file.printLines(30).toString());
    }
}
