package Zadanie_2;

public interface IConfiguration {
    String readLine();
    void writeLine(String line);
    void closeFile();
}
