package Zadanie_2;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Configuration implements IConfiguration{
    private BufferedReader reader;
    private PrintWriter writer;
    private List<String> list = new ArrayList<String>();

    public Configuration() {
        openFile();
    }

    public void openFile(){
        try {
            reader = new BufferedReader(new FileReader("test3.txt"));
            writer = new PrintWriter("test3.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeFile(){
        try {
            reader.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readLine(){
        try {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void writeLine(String line){
        writer.println(line);
        //punkty zycia
        //mana
        /*
        inne ustawienia maja jeszcze armor
        i jak wczytujesz ktores ustawienia to sie plik pisze od nowa
         */
    }
}
